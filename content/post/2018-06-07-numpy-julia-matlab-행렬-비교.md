---
title: "NumPy, Julia, MATLAB 행렬 비교"
date: 2018-06-07T00:56:00+09:00
# series: Numpy vs Julia vs MATLAB
tags: [Python, NumPy, Julia, MATLAB, Octave]
---

아래 내용은 Python 3.6 + NumPy 1.14, Julia 0.7.0-alpha, Octave 4.2에서 테스트하였습니다.
(이전 버전에서도 안 되는 부분은 거의 없습니다.)
Python에서 NumPy를 `import numpy as np`로 불러 왔다고 가정합니다.

## 벡터 구조

NumPy, Julia와는 달리 MATLAB에는 벡터(1차원) 구조가 따로 존재하지 않고 모두 차원이 2 이상인 행렬입니다.
심지어 `1`이라고만 쳐도, 실제로는 2차원 1 by 1 행렬입니다.
대신 많은 경우 1 by 1 행렬, $m$ by 1 행렬, 1 by $n$ 행렬을 특별히 스칼라, 열벡터, 행벡터로 해석하여 다르게 처리합니다.

```octave
% MATLAB
>> ndims(1)  % MATLAB에는 스칼라나 1차원 벡터가 따로 존재하지 않음
ans =  2
>> size(1)
ans =
   1   1

>> a = zeros(3, 1);
>> ndims(a)
ans =  2
>> size(a)
ans =
   3   1

>> sum([1 2 3; 4 5 6])  % 각 열의 합
ans =
   5   7   9
>> sum([1; 2; 3])  % 각 열의 합(열이 하나 뿐임)
ans =  6
>> sum([1 2 3])  % 1 by n 행렬(행벡터)인 경우 특수하게 행의 합
ans =  6
```

특히 1 by $n$ 행렬(행벡터)을 벡터가 아닌 정말로 행렬로 처리해야 할 때에 주의가 필요합니다.
예를 들어 $m$ by 3 행렬에서 각 열의 합을 구하고 싶은데 $m$이 1일 가능성이 있는 경우, 명시적으로 방향을 지정해 주어야 합니다.
($m$이 0인 경우에는 행렬처럼 처리됩니다.)

{{< highlight octave "hl_lines=5-6 14-16" >}}
% MATLAB
>> sum(zeros(0, 3))
ans =
   0   0   0
>> sum([1 2 3])
ans =  6
>> sum([1 2 3; 4 5 6])
ans =
   5   7   9

>> sum(zeros(0, 3), 1)
ans =
   0   0   0
>> sum([1 2 3], 1)
ans =
   1   2   3
>> sum([1 2 3; 4 5 6], 1)
ans =
   5   7   9
{{< /highlight >}}

반면 NumPy와 Julia에서는 벡터(1차원) 구조가 따로 존재합니다.

## 자료형과 크기

| | NumPy | Julia | MATLAB |
|---|---|---|---|
| `a` 행렬의 원소의 자료형 | `a.dtype` | `eltype(a)` | `class(a)` |
| `a` 행렬의 크기 | `a.shape` | `size(a)` | `size(a)` |
| `a` 행렬의 `n`번째 차원의 길이 | `a.shape[n]` | `size(a, n)` | `size(a, n)` |
| `a` 행렬의 차원 | `a.ndim` | `ndims(a)` | `ndims(a)` |

## 벡터와 행렬의 메모리 할당과 초기화

- NumPy의 경우 행렬 크기를 튜플(tuple)로 넘겨 주어야 합니다.
  즉, (1차원인 경우를 제외하면) 괄호를 하나 더 쳐 주어야 합니다.
- 인자를 스칼라 하나만 넘겨줄 때(예: `zeros(3)`) NumPy, Julia와 MATLAB에서 의미가 다릅니다.

| | NumPy | Julia | MATLAB |
|---|---|---|---|
| 길이가 3인 영벡터 | **`np.zeros(3)`**<br/>`np.zeros((3,))` | **`zeros(3)`** | `zeros(3, 1)`<br/>(실제로는 3 by 1 영**행렬**) |
| 1 by 3 영행렬 | `np.zeros((1, 3))` | `zeros(1, 3)` | `zeros(1, 3)` |
| 3 by 3 영행렬 | `np.zeros((3, 3))` | `zeros(3, 3)` | **`zeros(3)`**<br/>`zeros(3, 3)` |
| 3 by 4 영행렬 | `np.zeros((3, 4))` | `zeros(3, 4)` | `zeros(3, 4)` |
| `a`와 모양이 같은 영행렬 | `np.zeros(a.shape)` | `zeros(a)` | `zeros(size(a))` |
| |
| 모든 원소가 1인 3 by 4 행렬 | `np.ones((3, 4))` | `ones(3, 4)` | `ones(3, 4)` |
| 모든 원소가 42인 3 by 4 행렬 | `np.full((3, 4), 42)` | `fill(42, 3, 4)`<br/>`fill(42, (3, 4))` | `42 + zeros(3, 4)` |
| `a`의 모든 원소를 42로 채우기 | `a.fill(42)`<br/>`a[:] = 42` | `fill!(a, 42)`<br/>`a .= 42`<br/>`a[:] = 42` | `a(:) = 42` |
| |
| 초기화되지 않은 3 by 4 행렬 | `np.empty((3, 4))` | `Matrix{Float64}(undef, 3, 4)` | (없음) |
| `a`와 크기가 같은 초기화되지 않은 행렬 | `np.empty_like(a)` | `similar(a)` | (없음) |
| |
| 선형 간격 벡터(벡터 길이를 알 때) | `np.linspace(2.0, 3.0, num=5)` | `range(2.0, stop=3.0, length=5)`[^julia-range] | `linspace(2, 3, 5)`<br/>(1 by 5 행렬) |
| 선형 간격 벡터(원소간 간격을 알 때) | `np.arange(2.0, 3.0, 0.25)` | `2.0:0.25:3.0`[^julia-range] | `2:0.25:3`<br/>(1 by 5 행렬) |

[^julia-range]: 사실은 range 오브젝트이고, 명시적으로 벡터로 바꾸려면 `collect(2.0:0.25:3.0)`처럼 `collect()` 함수를 사용해야 합니다. 하지만 대부분의 경우 다른 연산을 할 때 자동으로 처리되고, 명시적으로 바꿔야 하는 경우는 많지 않습니다.

## 단위행렬

셋 모두 `eye()` 함수(혹은 메소드)를 사용하는데, Julia의 경우 버전 0.6부터 독특한 개념을 사용합니다.
(그래서 Julia에서는 `eye()` 함수가 deprecated되었습니다.)
MATLAB에서 `zeros()`는 거의 preallocation에 쓰이고, 단위행렬도 거의 characteristic equation 등 특수한 경우에 쓰이는 경우가 많습니다.
개인적으로 단위행렬을 $n^2$ 개의 수를 다 저장해야 하는 dense 행렬 자료 구조로 저장해야 할 일은 거의 없었습니다.
Julia에서는 [uniform scailing operator](https://docs.julialang.org/en/latest/stdlib/LinearAlgebra/#The-uniform-scaling-operator-1)라는 개념을 도입하였습니다.
간단히 말하면, 단위행렬이 필요한 곳에 단순히 `I`만 적으면 대부분의 경우에서 크기를 알아서 처리합니다.

```julia
# Julia
julia> a = [1 2; 3 4];
julia> a - 2I
2×2 Array{Int64,2}:
 -1  2
  3  2

julia> b = [1 2 3; 4 5 6; 7 8 9];
julia> b - 5I
3×3 Array{Int64,2}:
 -4  2  3
  4  0  6
  7  8  4
```
